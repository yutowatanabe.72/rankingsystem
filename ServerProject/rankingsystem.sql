-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 
-- サーバのバージョン： 10.4.6-MariaDB
-- PHP のバージョン: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `techtest`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `rankingsystem`
--

CREATE TABLE `rankingsystem` (
  `Id` int(11) NOT NULL,
  `都道府県名` varchar(30) NOT NULL,
  `Population` int(11) NOT NULL,
  `Face` float NOT NULL,
  `Populationdensity` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `rankingsystem`
--

INSERT INTO `rankingsystem` (`Id`, `都道府県名`, `Population`, `Face`, `Populationdensity`) VALUES
(1, 'いばらきけん', 2882943, 6097.33, 472.82),
(2, 'とちぎけん', 1952926, 6408.09, 304.76),
(3, 'ぐんまけん', 1949440, 6362.28, 306.41),
(4, 'さいたまけん', 7322645, 3797.75, 1928.15),
(5, 'ちばけん', 6268585, 5157.61, 1215.41),
(6, 'とうきょうと', 13843403, 2193.96, 6309.78),
(7, 'かながわけん', 9179835, 2416.16, 3799.35);

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `rankingsystem`
--
ALTER TABLE `rankingsystem`
  ADD PRIMARY KEY (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
