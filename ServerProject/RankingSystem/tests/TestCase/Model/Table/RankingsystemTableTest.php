<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RankingsystemTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RankingsystemTable Test Case
 */
class RankingsystemTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RankingsystemTable
     */
    public $Rankingsystem;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Rankingsystem'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Rankingsystem') ? [] : ['className' => RankingsystemTable::class];
        $this->Rankingsystem = TableRegistry::getTableLocator()->get('Rankingsystem', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Rankingsystem);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
