<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Rankingsystem Entity
 *
 * @property int $Id
 * @property string $都道府県名
 * @property int $人口
 * @property float $面積
 * @property float $人口密度
 */
class Rankingsystem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '都道府県名' => true,
        '人口' => true,
        '面積' => true,
        '人口密度' => true
    ];
}
