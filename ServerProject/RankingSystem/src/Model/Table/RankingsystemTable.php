<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Rankingsystem Model
 *
 * @method \App\Model\Entity\Rankingsystem get($primaryKey, $options = [])
 * @method \App\Model\Entity\Rankingsystem newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Rankingsystem[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingsystem saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Rankingsystem patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Rankingsystem findOrCreate($search, callable $callback = null, $options = [])
 */
class RankingsystemTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rankingsystem');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('都道府県名')
            ->maxLength('都道府県名', 30)
            ->requirePresence('都道府県名', 'create')
            ->notEmptyString('都道府県名');

        $validator
            ->integer('人口')
            ->requirePresence('人口', 'create')
            ->notEmptyString('人口');

        $validator
            ->numeric('面積')
            ->requirePresence('面積', 'create')
            ->notEmptyString('面積');

        $validator
            ->numeric('人口密度')
            ->requirePresence('人口密度', 'create')
            ->notEmptyString('人口密度');

        return $validator;
    }
}
