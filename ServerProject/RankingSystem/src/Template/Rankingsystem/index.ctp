<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rankingsystem[]|\Cake\Collection\CollectionInterface $rankingsystem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
    </ul>
</nav>
<div class="rankingsystem index large-9 medium-8 columns content">
    <h3><?= __('都道府県ランキング（関東）') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('都道府県名') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Population') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Face') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Populationdensity') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($rankingsystem as $rankingsystem): ?>
            <tr>
                <td><?= $this->Number->format($rankingsystem->Id) ?></td>
                <td><?= h($rankingsystem->都道府県名) ?></td>
                <td><?= $this->Number->format($rankingsystem->Population) ?></td>
                <td><?= $this->Number->format($rankingsystem->Face) ?></td>
                <td><?= $this->Number->format($rankingsystem->Populationdensity) ?></td>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
