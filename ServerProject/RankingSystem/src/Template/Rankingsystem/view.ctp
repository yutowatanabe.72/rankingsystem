<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Rankingsystem $rankingsystem
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Rankingsystem'), ['action' => 'edit', $rankingsystem->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Rankingsystem'), ['action' => 'delete', $rankingsystem->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $rankingsystem->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Rankingsystem'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Rankingsystem'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="rankingsystem view large-9 medium-8 columns content">
    <h3><?= h($rankingsystem->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('都道府県名') ?></th>
            <td><?= h($rankingsystem->都道府県名) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($rankingsystem->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('人口') ?></th>
            <td><?= $this->Number->format($rankingsystem->人口) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('面積') ?></th>
            <td><?= $this->Number->format($rankingsystem->面積) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('人口密度') ?></th>
            <td><?= $this->Number->format($rankingsystem->人口密度) ?></td>
        </tr>
    </table>
</div>
