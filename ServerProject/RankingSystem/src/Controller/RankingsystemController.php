<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Rankingsystem Controller
 *
 * @property \App\Model\Table\RankingsystemTable $Rankingsystem
 *
 * @method \App\Model\Entity\Rankingsystem[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RankingsystemController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $rankingsystem = $this->paginate($this->Rankingsystem);

        $this->set(compact('rankingsystem'));
    }

    /**
     * View method
     *
     * @param string|null $id Rankingsystem id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $rankingsystem = $this->Rankingsystem->get($id, [
            'contain' => []
        ]);

        $this->set('rankingsystem', $rankingsystem);
    }
    
    public function rankingview()
    {
        $queryA	= $this->Rankingsystem->find('all');	//Databese queryを取得
        $queryA->order(['Populationdensity'=>'DESC']);
        $queryA->limit(3);
        $arrayA	= $queryA->toArray();			//配列に変換
        
        $this->set('arrayA', $arrayA[0]);
        $this->set('arrayB', $arrayA[1]);
        $this->set('arrayC', $arrayA[2]);
    	//debug($queryA);
    }
}
